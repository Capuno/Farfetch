#pragma once

std::string getPath() {
    char result[ PATH_MAX ];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
    std::string res = std::string(result);
    size_t lastdir = res.find_last_of("/");
    std::string dir = res.substr(0, lastdir+1);
    return dir;
}

int nLines(std::string s) {
    int n = 1;
    for (int i = 0; i < s.length(); ++i) {
        if (s[i] == '\n') {
            n++;
        }
    }
    return n;
}

void moveCursor(int x, int y) {
    while (x<0) {
        std::cout << std::endl;
        x++;
    }
    std::ostringstream xs, ys;
    xs << "\033[" << x-1 << "A";
    ys << "\033[50D\033[" << y << "C";
    std::cout << (x > 0 ? xs.str() : "") << (y > 0 ? ys.str() : "");
}

void rplc(std::string *s, const std::string &obj, const std::string &subs) {
    for(int i = 0; ; i += subs.length()) {
        i = s->find(obj, i);
        if(i == std::string::npos) break;
        s->erase(i, obj.length());
        s->insert(i, subs);
    }
}

bool fileExists(const char* path) {
    std::ifstream file(path);
    return file.good();
}

std::string parseColors(std::string output) {
    rplc(&output,"{RESET}",RESET);
    rplc(&output,"{BOLD}",BOLD);
    rplc(&output,"{DIM}",DIM);
    rplc(&output,"{BLACK}",BLACK);
    rplc(&output,"{RED}",RED);
    rplc(&output,"{GREEN}",GREEN);
    rplc(&output,"{YELLOW}",YELLOW);
    rplc(&output,"{BLUE}",BLUE);
    rplc(&output,"{MAGENTA}",MAGENTA);
    rplc(&output,"{CYAN}",CYAN);
    rplc(&output,"{GRAY}",GRAY);
    rplc(&output,"{WHITE}",WHITE);
    rplc(&output,"{BG_BLACK}",BG_BLACK);
    rplc(&output,"{BG_RED}",BG_RED);
    rplc(&output,"{BG_GREEN}",BG_GREEN);
    rplc(&output,"{BG_YELLOW}",BG_YELLOW);
    rplc(&output,"{BG_BLUE}",BG_BLUE);
    rplc(&output,"{BG_MAGENTA}",BG_MAGENTA);
    rplc(&output,"{BG_CYAN}",BG_CYAN);
    rplc(&output,"{BG_GRAY}",BG_GRAY);
    rplc(&output,"{BG_WHITE}",BG_WHITE);
    return output;
}
