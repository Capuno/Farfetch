## ARCH

```
       .
      /#\
     /###\
    /p^###\
   /##P^q##\
  /##(   )##\
 /###P   q#,^\
/P^         ^q\
```
*By [n0ah01](https://github.com/n0ah01)*

## DEBIAN

```
                                   
           _,met$$$$$gg.           
        ,g$$$$$$$$$$$$$$$P.        
      ,g$$P"     """Y$$.".         
     ,$$P'              `$$$.      
    ',$$P       ,ggs.     `$$b:    
    `d$$'     ,$P"'   .    $$$     
     $$P      d$'     ,    $$P     
     $$:      $$.   -    ,d$$'     
     $$;      Y$b._   _,d$P'       
     Y$$.    `.`"Y$$$$P"'          
     `$$b      "-.__               
      `Y$$                         
       `Y$$.                       
         `$$b.                     
           `Y$$b.                  
              `"Y$b._              
                  `"""             
```

## GHOST

```
   ▄▄▄
  ▀█▀██  ▄
▀▄██████▀
   ▀█████
      ▀▀▀▀▄
```
*By [n0ah01](https://github.com/n0ah01)*

## INVADER 1

```
  ▀▄   ▄▀
 ▄█▀███▀█▄
█▀███████▀█
▀ ▀▄▄ ▄▄▀ ▀
```
*By [n0ah01](https://github.com/n0ah01)*

## INVADER 2

```
 ▄▄▄████▄▄▄
███▀▀██▀▀███
▀▀███▀▀███▀▀
 ▀█▄ ▀▀ ▄█▀
```
*By [n0ah01](https://github.com/n0ah01)*

## INVADER 3

```
  ▄██▄
▄█▀██▀█▄
▀█▀██▀█▀
▀▄    ▄▀
```
*By [n0ah01](https://github.com/n0ah01)*

## TUX

```
       a8888b.
      d888888b.
      8P"YP"Y88
      8|o||o|88
      8'    .88
      8`._.' Y8.
     d/      `8b.
    dP   .    Y8b.
   d8:'  "  `::88b
  d8"         'Y88b
 :8P    '      :888
  8a.   :     _a88P
._/"Yaa_:   .| 88P|
\    YP"    `| 8P  `.
/     \.___.d|    .'
`--..__)8888P`._.'
```
*By [n0ah01](https://github.com/n0ah01)*

## UBUNTU

```
             .-.
       .-'``(|||)
    ,`\ \    `-`.
   /   \ '``-.   `
 .-.  ,       `___:
(:::) :        ___
 `-`  `       ,   :
   \   / ,..-`   ,
    `./ /    .-.`
       `-..-(   )
             `-`
```
*By [n0ah01](https://github.com/n0ah01)*