
run:
	@echo "[\033[32mBUILD\033[0m] ff binary file."
	@g++ main.cpp -s -std=c++11 -o build/ff
	@if [ ! -d ~/.config/farfetch ]; then echo "[\033[32mCREATE\033[0m] ~/.config/farfetch folder." && mkdir -p ~/.config/farfetch/; fi
	@if [ ! -f ~/.config/farfetch/settings.ini ]; then echo "[\033[32mMOVE\033[0m] ~/.config/farfetch files." && cp build/settings.ini build/skeleton.ascii ~/.config/farfetch/; fi

install:
	@echo "[\033[32mINSTALL\033[0m] ff binary file."; cp build/ff /usr/local/bin/

uninstall:
	@echo "[\033[31mDELETE\033[0m] /usr/local/bin/ff binary file (~/.config/farfetch not deleted)."
	@rm -rf /usr/local/bin/ff
